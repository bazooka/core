<?php

namespace Fantassin\Core\WordPress\Blocks;

/**
 * @deprecated
 */
interface HasBlockName {

  public function getName(): string;

}
